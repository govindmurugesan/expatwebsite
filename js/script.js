function playVid() { 
	document.getElementById("myVideo").play(); 

	document.getElementById("pause").style.display='block';
	document.getElementById("logo").style.display='none';
} 

function pauseVid() { 
	document.getElementById("myVideo").pause(); 
	document.getElementById("pause").style.display='none';
	document.getElementById("logo").style.display='block';
} 
function showOrHideMenu() {  
	var e = document.getElementById("toggle-menu-item");
   if (e.style.display == 'block' || e.style.display==''){
	   e.style.display = 'none';
   }else {
	   e.style.display = 'block';
   }
}


var bgVideo = $('#bg-video');
		if ( !device.tablet() && !device.mobile() ) {
			bgVideo.mb_YTPlayer();
			$('#bg-video-volume').click(function(e){
				var bgVideoVol = $(this);
				e.preventDefault();
				if( bgVideoVol.hasClass('icon-mute') ) {
					bgVideoVol.removeClass('icon-mute').addClass('icon-sound').attr('title', 'Mute');
					bgVideo.unmuteYTPVolume();
					bgVideo.setYTPVolume(5);
				} else {
					bgVideoVol.removeClass('icon-sound').addClass('icon-mute').attr('title', 'Unmute');
					bgVideo.muteYTPVolume();
				}
			});
			$('#bg-video-play').click(function(e){
				var bgVideoPlay= $(this);
				e.preventDefault();
				if( bgVideoPlay.hasClass('icon-pause') ) {
					bgVideoPlay.removeClass('icon-pause').addClass('icon-play').attr('title', 'Play');
					bgVideo.pauseYTP();
				} else {
					bgVideoPlay.removeClass('icon-play').addClass('icon-pause').attr('title', 'Pause');
					bgVideo.playYTP();
				}
			});
		} else {
			var posterUrl = bgVideo.data('poster');
			if ( posterUrl )
				$.backstretch(posterUrl);
			$('#bg-video-controls').hide();
		}function playVid() { 
	document.getElementById("myVideo").play(); 

	document.getElementById("pause").style.display='block';
	document.getElementById("logo").style.display='none';
} 

function pauseVid() { 
	document.getElementById("myVideo").pause(); 
	document.getElementById("pause").style.display='none';
	document.getElementById("logo").style.display='block';
} 
function showOrHideMenu() {  
	var e = document.getElementById("toggle-menu-item");
   if (e.style.display == 'block' || e.style.display==''){
	   e.style.display = 'none';
   }else {
	   e.style.display = 'block';
   }
}

var map;
window.onload=function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false' +
	'&signed_in=true&callback=initialize';
	document.body.appendChild(script);
}
//window.onload = loadScript;
function initialize() {
	var projectDetails = $('.project_content').attr('id');
	var contentString = "";
	var position = "";
	if(projectDetails === 'projectDetails1'){
		position =  new google.maps.LatLng(12.7888613, 77.661912);
		contentString = '<div class="maptext"><h2><b>LOCATE US</b></h2> <h4> #109/3, <strong>Jigani Hobli</strong>,</h4><h4> Anekal Taluk, Rajapura,</h4><h4> Bangalore- 562106 </h4></div>';
	} else if(projectDetails === 'projectDetails2'){
		position =  new google.maps.LatLng(12.8339997, 77.7827218);
		contentString = '<div class="maptext"><h2><b>LOCATE US</b></h2> <h4> <strong>Confident Aspire</strong>,</h4><h4>Sarjapur -Attibele Main Road, Near indus International School Sarjapur,</h4><h4> Karnataka - 562125 </h4></div>';
	} else if(projectDetails === 'projectDetails3'){
		position =  new google.maps.LatLng(12.8159023, 77.7886963);
		contentString = '<div class="maptext"><h2><b>LOCATE US</b></h2> <h4> <strong>Confident Aries</strong>,</h4><h4>BEML Cooperative Society Layout,</h4><h4> Karnataka - 562125 </h4></div>';
	} else if(projectDetails === 'projectDetails4'){
		position =  new google.maps.LatLng(12.971180, 77.597253);
		contentString = '<div class="maptext"><h2><b>LOCATE US</b></h2> <h4> <strong>Confident Aspire</strong>,</h4><h4>Sarjapur -Attibele Main Road, Near indus International School Sarjapur,</h4><h4> Karnataka - 562125 </h4></div>';
	} else if(projectDetails === 'projectDetails5'){
		position =  new google.maps.LatLng(12.971180, 77.597253);
		contentString = '<div class="maptext"><h2><b>LOCATE US</b></h2> <h4> <strong>Confident Aspire</strong>,</h4><h4>Sarjapur -Attibele Main Road, Near indus International School Sarjapur,</h4><h4> Karnataka - 562125 </h4></div>';
	}
  var mapOptions = {
	   center:position,
	   zoom: 16,
	   scrollwheel: false,
	   mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
	var marker = new google.maps.Marker({
        position: position,
        map: map,
        title:"#34, Embassy Diamante, Next to UB City, Vittal Mallya Road, Bangalore- 560001"
    });
	
	var infowindow = new google.maps.InfoWindow({
    content: contentString
	});
	infowindow.open(map,marker);
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });
	
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, "resize", function() {
	var center = map.getCenter();
	google.maps.event.trigger(map, "resize");
	map.setCenter(center); 
});

var bgVideo = $('#bg-video');
		if ( !device.tablet() && !device.mobile() ) {
			bgVideo.mb_YTPlayer();
			$('#bg-video-volume').click(function(e){
				var bgVideoVol = $(this);
				e.preventDefault();
				if( bgVideoVol.hasClass('icon-mute') ) {
					bgVideoVol.removeClass('icon-mute').addClass('icon-sound').attr('title', 'Mute');
					bgVideo.unmuteYTPVolume();
					bgVideo.setYTPVolume(5);
				} else {
					bgVideoVol.removeClass('icon-sound').addClass('icon-mute').attr('title', 'Unmute');
					bgVideo.muteYTPVolume();
				}
			});
			$('#bg-video-play').click(function(e){
				var bgVideoPlay= $(this);
				e.preventDefault();
				if( bgVideoPlay.hasClass('icon-pause') ) {
					bgVideoPlay.removeClass('icon-pause').addClass('icon-play').attr('title', 'Play');
					bgVideo.pauseYTP();
				} else {
					bgVideoPlay.removeClass('icon-play').addClass('icon-pause').attr('title', 'Pause');
					bgVideo.playYTP();
				}
			});
		} else {
			var posterUrl = bgVideo.data('poster');
			if ( posterUrl )
				$.backstretch(posterUrl);
			$('#bg-video-controls').hide();
		}